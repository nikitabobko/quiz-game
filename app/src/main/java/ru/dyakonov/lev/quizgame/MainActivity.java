package ru.dyakonov.lev.quizgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.bobko.sandbox.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	private int numberOfQuestionsForUser = 10;

	private TextView questionTextView;
	private Button nextButtonQuestion;
	private RadioButton[] answersRadioButtons = new RadioButton[4];
	private RadioGroup radioGroup;
	private Button skipQuestionButton;
	private int trueAnsweredCount;
	// current - текущий
	private QuestionAndPossibleAnswers currentQuestion;
	private QuestionGenerator generator = new QuestionGenerator();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Intent intent = getIntent();
		numberOfQuestionsForUser = intent.getIntExtra("numberOfQuestions",numberOfQuestionsForUser);

		questionTextView = findViewById(R.id.question_text_view);
		nextButtonQuestion = findViewById(R.id.next_question_button);
		radioGroup = findViewById(R.id.radio_group);
		skipQuestionButton = findViewById(R.id.skip_question_button);
		answersRadioButtons[0] = findViewById(R.id.button1);
		answersRadioButtons[1] = findViewById(R.id.button2);
		answersRadioButtons[2] = findViewById(R.id.button3);
		answersRadioButtons[3] = findViewById(R.id.button4);

		nextButtonQuestion.setOnClickListener(this);
		skipQuestionButton.setOnClickListener(this);
		nextButtonQuestion.setEnabled(false);
		for (int i = 0; i < answersRadioButtons.length; i++) {
			answersRadioButtons[i].setOnClickListener(this);
		}

		currentQuestion = generator.getNextQuestion();
		updateUIForNewGeneratedQuestion(null);
	}

	/**
	 * Обновить UI (User Inteface (Пользовательский интерфейс)) в соответствии с текущим вопрсом
	 */
	private void updateUIForNewGeneratedQuestion(Button buttonToShowUserThatQuestionsEnded) {
		if (currentQuestion == null) {
			nextButtonQuestion.setEnabled(false);
			skipQuestionButton.setEnabled(false);
			if (buttonToShowUserThatQuestionsEnded != null) {
				buttonToShowUserThatQuestionsEnded.setText("Вопросы закончились");
			}

			// Альтернативный вариант
			// Toast.makeText(this, "Вопросы закончились", Toast.LENGTH_SHORT).show();

		} else {
			if (generator.getQuestionCount() > numberOfQuestionsForUser) {
				Intent intent = new Intent(this, GameOverActivity.class);
				intent.putExtra("points", trueAnsweredCount);
				intent.putExtra("numberOfQuestionsForUser", numberOfQuestionsForUser);
				nextButtonQuestion.setEnabled(false);
				skipQuestionButton.setEnabled(false);
				startActivity(intent);
				finish();
				return;
			}
			questionTextView.setText(currentQuestion.getQuestion());

			currentQuestion.shuffle();
			for (int i = 0; i < answersRadioButtons.length; i++) {
				answersRadioButtons[i].setText(currentQuestion.getAnswerVariant(i));
			}

			for (int i = 0; i < answersRadioButtons.length; i++) {
				radioGroup.clearCheck();
			}
			nextButtonQuestion.setEnabled(false);
		}
	}

	/**
	 * Gets user answer on question
	 *
	 * @return user answer on question
	 */
	String getUserAnswer() {
		for (int i = 0; i < answersRadioButtons.length; i++) {
			if (answersRadioButtons[i].isChecked()) {
				return answersRadioButtons[i].getText().toString();
			}
		}
		return "";
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == nextButtonQuestion.getId()) {
			if (getUserAnswer().equals(currentQuestion.getTrueAnswer())) {
				trueAnsweredCount++;
			}
			currentQuestion = generator.getNextQuestion();
			updateUIForNewGeneratedQuestion(nextButtonQuestion);
		} else if (view.getId() == skipQuestionButton.getId()) {
			currentQuestion = generator.getNextQuestion();
			updateUIForNewGeneratedQuestion(skipQuestionButton);
		} else {
			nextButtonQuestion.setEnabled(true);
		}
	}
}
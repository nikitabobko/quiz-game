package ru.dyakonov.lev.quizgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bobko.sandbox.R;

public class GameOverActivity extends AppCompatActivity implements View.OnClickListener {

	private TextView userResultTextView;
	private Button newGameButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_over);

		userResultTextView = (TextView) findViewById(R.id.show_user_result);
		newGameButton = (Button) findViewById(R.id.new_game_button);

		Intent intent = getIntent();
		int trueAnsweredQuestion = intent.getIntExtra("points", 0);
		int numberOfQuestionsForUser = intent.getIntExtra("numberOfQuestionsForUser",0);

		userResultTextView.setText("Вы ответили правильно на " + trueAnsweredQuestion + " вопросов из "+numberOfQuestionsForUser+"!");
		newGameButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		Intent intent = new Intent(this, NumberOfQuestionPickerActivity.class);
		startActivity(intent);
		finish();
	}
}

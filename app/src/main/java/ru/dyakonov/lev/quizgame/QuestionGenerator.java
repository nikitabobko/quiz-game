package ru.dyakonov.lev.quizgame;

import java.util.Random;

/**
 * Created by valentindiaconov on 23/12/2017.
 */
public class QuestionGenerator {
	// static - свойство или метод, которое принадлежит не конкретному объекту, а целому классы
	// final - константа (т.е. ее значение не может меняться). Константы принято писать Caps Lock'ом
	private static final int NUMBER_OF_QUESTIONS = 20;
	private QuestionAndPossibleAnswers[] questions = new QuestionAndPossibleAnswers[NUMBER_OF_QUESTIONS];
	// Если вопрос уже был задан, то true
	private boolean[] alreadyAskedQuestions = new boolean[NUMBER_OF_QUESTIONS];
	private int numberOfNotYetAskedQuestions = NUMBER_OF_QUESTIONS;
	private int questionCount;

	public QuestionGenerator() {
		questions[0] = new QuestionAndPossibleAnswers("сколько иголок у моржа?",
				new String[]{
						"0",
						"4",
						"143",
						"1"
				});
		questions[1] = new QuestionAndPossibleAnswers("в каком возрвсте в росии идут в школу?",
				new String[]{
						"6-7",
						"5-6",
						"8-9",
						"11-12"
				});
		questions[2] = new QuestionAndPossibleAnswers("сколько на земле материков, чье название начинается на букву а?",
				new String[]{
						"5",
						"4",
						"6",
						"8"
				});
		questions[3] = new QuestionAndPossibleAnswers("сколько будет 2+2*2?",
				new String[]{
						"6",
						"8",
						"10",
						"12"
				});
		questions[4] = new QuestionAndPossibleAnswers("Какая планета самая близкая к солнцу?",
				new String[]{
						"Меркурий",
						"Венера",
						"Марс",
						"Земля"
				});
		questions[5] = new QuestionAndPossibleAnswers("как называется сайт для просмотра видео?",
				new String[]{
						"youtube",
						"яндекс",
						"google",
						"mail.ru"
				});
		questions[6] = new QuestionAndPossibleAnswers("сколько иголок у бомжа?",
				new String[]{
						"0",
						"4",
						"143",
						"1"
				});
		questions[7] = new QuestionAndPossibleAnswers("сколько лап у паука?",
				new String[]{
						"8",
						"7",
						"4",
						"6",
				});
		questions[8] = new QuestionAndPossibleAnswers("как называется операционная система от google?",
				new String[]{
						"android",
						"ios",
						"windows",
						"macOS"
				});
		questions[9] = new QuestionAndPossibleAnswers("сколько камней бесконечности в киновселенной марвел?",
				new String[]{
						"6",
						"0",
						"5",
						"7"
				});
		questions[10] = new QuestionAndPossibleAnswers("сколько иголок у ежа?",
				new String[]{
						"не знаю",
						"0",
						"4",
						"143",
				});
		questions[11] = new QuestionAndPossibleAnswers("Чему равен дискриминант?",
				new String[]{
						"b^2 - 4ac",
						"4ac - b^2",
						"14",
						"не знаю"
				});
		questions[12] = new QuestionAndPossibleAnswers("Что такое равнобедренный треугольник?",
				new String[]{
						"У которого две стороны равны",
						"У которого один из углов 30 градусов ",
						"У которого один угол из уголов 90 градусов",
						"У которого сторона равна 5 см"
				});

		questions[13] = new QuestionAndPossibleAnswers("Как называется язык программирования, на котором написана эта программа?",
				new String[]{
						"Java",
						"Python",
						"C++",
						"JavaScript"
				});
		questions[14] = new QuestionAndPossibleAnswers("\"Пришел, увидел, победил\", чья цитата?",
				new String[]{
						"Юлий Цезарь",
						"Пенелопа Крус",
						"Иван Грозный",
						"Наполеон"
				});
		questions[15] = new QuestionAndPossibleAnswers("Определение синуса в прямоугольном треугольнике?",
				new String[]{
						"Отношение противолежащего катета к гипотенузе",
						"Отношение прилежащего катета к гипотенузе",
						"Квадрат гипотенузы",
						"Квадрат противолежащего катета"
				});
		questions[16] = new QuestionAndPossibleAnswers("Что тяжелее килограмм ваты или килограмм железа?",
				new String[]{
						"Здесь нет правильного варианта ответа",
						"Килограмм ваты ",
						"Килограмм железа",
						"Их нельзя сравнивать"
				});
		questions[17] = new QuestionAndPossibleAnswers("Сколько раз цифра 3 используется в записи двузначных чисел?",
				new String[]{
						"19",
						"13",
						"20",
						"21"
				});
		questions[18] = new QuestionAndPossibleAnswers("Сколько нужно градусов, чтобы вода закипела?",
				new String[]{
						"100",
						"200",
						"105",
						"90"
				});
		questions[19] = new QuestionAndPossibleAnswers("Сколько секунд в часе?",
				new String[]{
						"3600",
						"4000",
						"3000",
						"4600"
				});

	}

	public int getQuestionCount() {
		return questionCount;
	}

	public QuestionAndPossibleAnswers getNextQuestion() {
		questionCount++;
		Random random = new Random();
		if (numberOfNotYetAskedQuestions == 0) {
			return null;
		}
		int a = random.nextInt(numberOfNotYetAskedQuestions);
		for (int i = 0; i < NUMBER_OF_QUESTIONS; i++) {
			if (alreadyAskedQuestions[i] == false) {
				if (a == 0) {
					numberOfNotYetAskedQuestions--;
					alreadyAskedQuestions[i] = true;
					return questions[i];
				} else {
					a--;
				}
			}
		}
		return null;

		// "грязный вариант"
		/*
		Random random = new Random();
		return questions[random.nextInt(NUMBER_OF_QUESTIONS)];
		*/
	}
}


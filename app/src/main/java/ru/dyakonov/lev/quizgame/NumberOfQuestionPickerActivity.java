package ru.dyakonov.lev.quizgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.example.bobko.sandbox.R;

public class NumberOfQuestionPickerActivity extends AppCompatActivity implements View.OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_number_of_question_picker);

		Button fiveQuestionNumber = findViewById(R.id.fiveQuestionsButton);
		Button tenQuestionNumber = findViewById(R.id.tenQuestionsButton);
		Button fifteenQuestionsButton = findViewById(R.id.fifteenQuestionsButton);

		fiveQuestionNumber.setOnClickListener(this);
		tenQuestionNumber.setOnClickListener(this);
		fifteenQuestionsButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		Button button = (Button) view;
		String buttonText = button.getText().toString();
		int value = Integer.parseInt(buttonText);
		finish();
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("numberOfQuestions" ,value);
		startActivity(intent);
	}
}

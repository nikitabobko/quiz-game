package ru.dyakonov.lev.quizgame;

import java.util.Random;

public class QuestionAndPossibleAnswers {
	private String question;

	private String[] answerVariants;

	private String trueAnswer;

	// Конструктор класса
	public QuestionAndPossibleAnswers(String question, String answerVariants[]) {
		if (answerVariants.length != 4) {
			throw new RuntimeException("length should be 4");
		}
		this.question = question;
		this.answerVariants = answerVariants;
		this.trueAnswer = answerVariants[0];
	}

	private static String[] shuffleArray(String[] array) {
		Random rgen = new Random();
		for (int i = 0; i < array.length; i++) {
			int randomPosition = rgen.nextInt(array.length);
			String temp = array[i];
			array[i] = array[randomPosition];
			array[randomPosition] = temp;
		}
		return array;
	}

	/**
	 * Замешать варианты ответов
	 */
	public void shuffle() {
		shuffleArray(answerVariants);
	}

	public String getQuestion() {
		return question;
	}

	public String getAnswerVariant(int i) {
		return answerVariants[i];
	}

	public String getTrueAnswer() {
		return trueAnswer;
	}

	// Возвращает массив строк String[]
	public String[] getAnswerVariants() {
		return answerVariants;
	}
}